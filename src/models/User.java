package models;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;

public class User {

    private UUID id;
    private final String firstName;
    private final String lastNam;
    private final String email;
    private final String password;


    public User(String firstName, String lastNam, String email, String password) {
        this.firstName = firstName;
        this.lastNam = lastNam;
        this.email = email;
        this.password = password;
    }

    public User(String userAsString) {

        String[] a = userAsString.split(", ");

        this.id = UUID.fromString(a[0]);
        this.firstName = a[1];
        this.lastNam = a[2];
        this.email = a[3];
        this.password = a[4];


    }

    public User(UUID id, String firstName, String lastNam, String email, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastNam = lastNam;
        this.email = email;
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastNam() {
        return lastNam;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return
                id +
                        ", " + firstName +
                        ", " + lastNam +
                        ", " + email +
                        ", " + password;
    }
}
