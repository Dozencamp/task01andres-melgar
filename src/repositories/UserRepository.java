package repositories;

import models.User;
import repositories.CrudRepository;

import java.util.UUID;

public interface UserRepository extends CrudRepository<UUID, User> {

}
