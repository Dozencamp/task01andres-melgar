package services;

import dto.SignUpForm;

public interface UserService {
    void signUp(SignUpForm form);

    boolean emailValidation(String email);

}
