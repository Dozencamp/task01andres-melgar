package app;

import dto.SignUpForm;
import mappers.Mappers;
import models.User;
import repositories.UserRepository;
import repositories.UserRepositoryimpl;
import services.*;

import java.util.UUID;

public class Main {
    public static void main(String a[]) {

        UserRepository repo = new UserRepositoryimpl("users.txt");

        SignUpForm form = new SignUpForm("Andres", "Melgar", "andresm@hotmail.com", "54321");

        UserService service = new UserServiceImpl(repo, Mappers::FromSignUpForm);
        service.signUp(form);


    }
}
