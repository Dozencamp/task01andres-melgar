package services;

import dto.SignUpForm;
import repositories.UserRepository;
import models.User;
import repositories.UserRepositoryimpl;

import java.rmi.server.UID;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Pattern;

public class UserServiceImpl implements UserService {

    public final UserRepository userRepository;

    public final Function<SignUpForm, User> toUserMapper;

    public UserServiceImpl(UserRepository userRepository, Function<SignUpForm, User> toUserMapper) {
        this.userRepository = userRepository;
        this.toUserMapper = toUserMapper;
    }


    public boolean emailValidation(String email) {
        String emailRegex = "^(.+)@(\\S+)$";
        return Pattern.compile(emailRegex)
                .matcher(email)
                .matches();
    }


    public void signUp(SignUpForm form) {
        User user = toUserMapper.apply(form);
        UUID id = UUID.randomUUID();
        if (emailValidation(user.getEmail())) {
            user.setId(id);
            userRepository.save(user);

        } else {
            System.out.println("invalid email");
        }
    }
}
